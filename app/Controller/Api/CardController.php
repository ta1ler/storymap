<?php

namespace Ta1ler\Storymap\Controller\Api;

use Ta1ler\Storymap\Controller\Controller;
use Ta1ler\Storymap\Entity\Token;
use Ta1ler\Storymap\Entity\User;
use Ta1ler\Storymap\Entity\IndexCard;

class CardController extends Controller {

  public function getCard($cardId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $card = $this->storyManager->getCard($cardId, $token->getUser());

    if (is_null($card)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $response_data = array(
      'id' => $card->getId(),
      'title' => $card->getTitle(),
      'description' => $card->getDescription(),
      'bgColor' => $card->getBgColor(),
      'col' => $card->getCol(),
      'row' => $card->getRow(),
      'authorId' => $card->getAuthor()->getId(),
      'storyId' => $card->getStory()->getId()
    );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function getStoryCards($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $story = $this->storyManager->getStory($storyId, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $cards = $this->storyManager->getStoryCards($story);

    if (is_null($cards)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = array();

    foreach ($cards as $card) {
      array_push($response_data, array(
        'id' => $card->getId(),
        'title' => $card->getTitle(),
        'description' => $card->getDescription(),
        'bgColor' => $card->getBgColor(),
        'col' => $card->getCol(),
        'row' => $card->getRow(),
        'authorId' => $card->getAuthor()->getId(),
        'storyId' => $card->getStory()->getId()
      ));
    }

    return $this->generateJsonResponse(['cards' => $response_data], 200);
  }

  public function createCard($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $story = $this->storyManager->getStory($storyId, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $request = $this->requestStack->getCurrentRequest();
    $json = $request->getContent();
    $content = json_decode($json, true);

    $title = $content['title'];
    $description = $content['description'];
    $bgColor = $content['bgColor'];
    $col = $content['col'];
    $row = $content['row'];

    $card = $this->storyManager->createCard(
      $title,
      $description,
      $bgColor,
      $col,
      $row,
      $story,
      $token->getUser()
    );

    if (is_null($card)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = array(
      'id' => $card->getId(),
      'title' => $card->getTitle(),
      'description' => $card->getDescription(),
      'bgColor' => $card->getBgColor(),
      'col' => $card->getCol(),
      'row' => $card->getRow(),
      'authorId' => $card->getAuthor()->getId(),
      'storyId' => $card->getStory()->getId()
    );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function changeCard($cardId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $card = $this->storyManager->getCard($cardId, $token->getUser());

    if (is_null($card)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $request = $this->requestStack->getCurrentRequest();
    $json = $request->getContent();
    $content = json_decode($json, true);

    $title = $content['title'];
    $description = $content['description'];
    $bgColor = $content['bgColor'];
    $col = $content['col'];
    $row = $content['row'];

    $card = $this->storyManager->changeCard(
      $card,
      $title,
      $description,
      $bgColor,
      $col,
      $row
    );

    if (is_null($card)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = array(
      'id' => $card->getId(),
      'title' => $card->getTitle(),
      'description' => $card->getDescription(),
      'bgColor' => $card->getBgColor(),
      'col' => $card->getCol(),
      'row' => $card->getRow(),
      'authorId' => $card->getAuthor()->getId(),
      'storyId' => $card->getStory()->getId()
    );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function deleteCard($cardId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    if (!$this->storyManager->deleteCard($cardId, $token->getUser())) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    return $this->generateJsonResponse([], 200);
  }
}