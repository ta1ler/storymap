<?php

namespace Ta1ler\Storymap\Controller\Api;

use Ta1ler\Storymap\Controller\Controller;
use Ta1ler\Storymap\Entity\Token;
use Ta1ler\Storymap\Entity\User;

class StoryController extends Controller {

  public function getUserStories($userId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $user = $this->sessionManager->getUser($userId);

    if (is_null($user)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $stories = $this->storyManager->getUserStories($user, $token);

    if (is_null($stories)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = array();

    foreach ($stories as $story) {
      array_push($response_data, array(
        'id' => $story->getId(),
        'title' => $story->getTitle(),
        'mapWidth' => $story->getMapWidth(),
        'mapHeight' => $story->getMapHeight()
      ));
    }

    return $this->generateJsonResponse(['stories' => $response_data], 200);
  }

  public function createStory($userId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $user = $this->sessionManager->getUser($userId);

    if (is_null($user)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $request = $this->requestStack->getCurrentRequest();
    $json = $request->getContent();
    $content = json_decode($json, true);

    $title = $content['title'];

    $story = $this->storyManager->createStory($title, $user, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = array(
      'id' => $story->getId(),
      'title' => $story->getTitle(),
      'mapWidth' => $story->getMapWidth(),
      'mapHeight' => $story->getMapHeight()
    );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function getStory($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $story = $this->storyManager->getStory($storyId, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $response_data = array(
      'id' => $story->getId(),
      'title' => $story->getTitle(),
      'mapWidth' => $story->getMapWidth(),
      'mapHeight' => $story->getMapHeight()
    );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function deleteStory($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    if (!$this->storyManager->deleteStory($storyId, $token)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    return $this->generateJsonResponse([], 200);
  }
}