<?php

namespace Ta1ler\Storymap\Controller\Api;

use Ta1ler\Storymap\Controller\Controller;
use Ta1ler\Storymap\Service\SessionManager;
use Ta1ler\Storymap\Entity\Token;

class TokenController extends Controller {

  public function signin() {
    if (!($token = $this->basicAuth())) {

      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }

      $token = $this->sessionManager->updateToken($token);
    }

    return $this->generateJsonResponse(['token' => $token->getToken()], 200);
  }

  public function signout() {
    if (!($token = $this->tokenAuth())) {
      return $this->generateJsonResponse([], 401, 'Unauthorized');
    }

    if ($this->sessionManager->deleteToken($token)) {
      return $this->generateJsonResponse([], 200);
    }

    return $this->generateJsonResponse([], 500, 'Internal Server Error');
  }
}