<?php

namespace Ta1ler\Storymap\Controller\Api;

use Ta1ler\Storymap\Controller\Controller;
use Ta1ler\Storymap\Entity\User;
use Ta1ler\Storymap\Entity\Token;

class UserController extends Controller {

  public function signup() {
    $request = $this->requestStack->getCurrentRequest();
    $json = $request->getContent();
    $content = json_decode($json, true);

    $email = $content['email'];
    $password = $content['password'];
    $firstName = $content['firstName'];
    $secondName = $content['secondName'];
    $avatarPath = null; // NOT IMPLEMENTED YET

    $data = $this->sessionManager->registerUser($email, $password, $firstName, $secondName, $avatarPath);

    if (is_null($data)) {
      return $this->generateJsonResponse([], 409, 'Conflict');
    }

    $response_data = array(
      'token' => $data['token']->getToken(),
      'user' => array(
        'id' => $data['user']->getId(),
        'firstName' => $data['user']->getFirstName(),
        'secondName' => $data['user']->getSecondName(),
        'avatarPath' => $data['user']->getAvatarPath()
        )
      );

    return $this->generateJsonResponse($response_data, 200);
  }

  public function getCurrentUser() {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $user = $token->getUser();

    if (is_null($user)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    $response_data = ['user' => array(
      'id' => $user->getId(),
      'firstName' => $user->getFirstName(),
      'secondName' => $user->getSecondName(),
      'avatarPath' => $user->getAvatarPath()
      )];

    return $this->generateJsonResponse($response_data, 200);
  }

  public function getUser($userId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $user = $this->sessionManager->getUser($userId);

    if (is_null($user)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $response_data = ['user' => array(
      'id' => $user->getId(),
      'firstName' => $user->getFirstName(),
      'secondName' => $user->getSecondName(),
      'avatarPath' => $user->getAvatarPath()
      )];

    return $this->generateJsonResponse($response_data, 200);
  }

  public function deleteUser($userId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $user = $this->sessionManager->getUser($userId);

    if (is_null($user)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    if ($this->sessionManager->deleteUser($user, $token)) {
      return $this->generateJsonResponse([], 200);
    }

    return $this->generateJsonResponse([], 500, 'Internal Server Error');
  }

  public function getStoryUsers($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $story = $this->storyManager->getStory($storyId, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $users = $this->storyManager->getUsers($story);

    if (is_null($users)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $response_data = array();

    foreach ($users as $user) {
      array_push($response_data, array(
        'id' => $user->getId(),
        'firstName' => $user->getFirstName(),
        'secondName' => $user->getSecondName(),
        'avatarPath' => $user->getAvatarPath()
      ));
    }

    return $this->generateJsonResponse(['users' => $response_data], 200);
  }

  public function changeStoryUsers($storyId) {
    if (!($token = $this->basicAuth())) {
      if (!($token = $this->tokenAuth())) {
        return $this->generateJsonResponse([], 401, 'Unauthorized');
      }
    }

    $story = $this->storyManager->getStory($storyId, $token);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 404, 'Not Found');
    }

    $request = $this->requestStack->getCurrentRequest();
    $json = $request->getContent();
    $content = json_decode($json, true);

    $usersArray = $content['users'];

    $users = array();

    foreach ($usersArray as $userId) {
      $user = $this->sessionManager->getUser($userId);

      if (is_null($user)) {
        return $this->generateJsonResponse([], 404, 'Not Found');
      }

      array_push($users, $user);
    }

    $story = $this->storyManager->changeUsers($story, $users);

    if (is_null($story)) {
      return $this->generateJsonResponse([], 500, 'Internal Server Error');
    }

    return $this->generateJsonResponse(['users' => $usersArray], 200);
  }
}