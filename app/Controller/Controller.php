<?php

namespace Ta1ler\Storymap\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

use Ta1ler\Storymap\Service\SessionManager;
use Ta1ler\Storymap\Service\StoryManager;

class Controller {
  protected $requestStack;
  protected $sessionManager;
  protected $storyManager;
  protected $viewPath;

  public function __construct(RequestStack $requestStack, SessionManager $sessionManager, StoryManager $storyManager, string $viewPath) {
    $this->requestStack = $requestStack;
    $this->sessionManager = $sessionManager;
    $this->storyManager = $storyManager;
    $this->viewPath = $viewPath;
  }

  protected function basicAuth() {
    $request = $this->requestStack->getCurrentRequest();
    $authHeader = $request->headers->get('authorization');

    if (is_null($authHeader)) {
      return;
    }

    if (!preg_match('/Basic\s(\S+)/', $authHeader, $matches)) {
      return;
    }

    $authValue = explode(':', base64_decode($matches[1]));

    $email = $authValue[0];
    $password = $authValue[1];

    return $this->sessionManager->loginWithPassword($email, $password);
  }

  protected function tokenAuth() {
    $request = $this->requestStack->getCurrentRequest();
    $authHeader = $request->headers->get('authorization');

    if (is_null($authHeader)) {
      return;
    }

    if (!preg_match('/Bearer\s(\S+)/', $authHeader, $matches)) {
      return;
    }

    $token = $matches[1];

    return $this->sessionManager->loginWithToken($token);
  }

  public function generateHtmlResponse($view, $template, $data=[]) {
    extract($data);
    require_once ($this->viewPath . $template);
    return new Response('', 200);
  }

  public function generateJsonResponse($data = [], $code = 200, $error = '') {
    $content = [
      'success' => ($error == ''),
      'data' => $data,
      'error' => $error
    ];
    return new JsonResponse($content, $code);
  }

}