<?php

namespace Ta1ler\Storymap\Controller;

class DebugController extends Controller {

  public function indexAction() {

    $view = 'debug_view.php';
    $title = 'Debug';
    return $this->generateHtmlResponse($view, 'debug_template.php', [
            'title'=> $title,
        ]);
  }
}