<?php

namespace Ta1ler\Storymap\DataMapper;

use Doctrine\DBAL\Connection;

use Ta1ler\Storymap\Entity\IndexCard;

class CardMapper {

  private $connection;
  private $userMapper;
  private $storyMapper;

  public function __construct(Connection $connection, UserMapper $userMapper, StoryMapper $storyMapper) {
    $this->connection = $connection;
    $this->userMapper = $userMapper;
    $this->storyMapper = $storyMapper;
  }

  public function findCardById($id) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'ic.card_id',
        'ic.title',
        'ic.description',
        'ic.bg_color',
        'ic.col',
        'ic.row',
        'ic.author_id',
        'ic.story_id')
      ->from('IndexCard', 'ic')
      ->where('ic.card_id = ?')
      ->setParameter(0, $id);
    $result = $queryBuilder->execute()->fetch();

    if ($result === false) {
      return;
    }

    $author = $this->userMapper->findUserById($result['author_id']);
    $story = $this->storyMapper->findStoryById($result['story_id']);

    $card = new IndexCard();
    $card
      ->setId($result['card_id'])
      ->setTitle($result['title'])
      ->setDescription($result['description'])
      ->setBgColor($result['bg_color'])
      ->setCol($result['col'])
      ->setRow($result['row'])
      ->setAuthor($author)
      ->setStory($story);

    return $card;
  }

  public function findCardsByStory($storyId) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'ic.card_id',
        'ic.title',
        'ic.description',
        'ic.bg_color',
        'ic.col',
        'ic.row',
        'ic.author_id',
        'ic.story_id')
      ->from('IndexCard', 'ic')
      ->where('ic.story_id = ?')
      ->setParameter(0, $storyId);
    $result = $queryBuilder->execute()->fetchAll();

    if ($result === false) {
      return;
    }

    $cards = array();

    foreach ($result as $value) {
      $author = $this->userMapper->findUserById($value['author_id']);
      $story = $this->storyMapper->findStoryById($value['story_id']);

      $card = new IndexCard();
      $card
        ->setId($value['card_id'])
        ->setTitle($value['title'])
        ->setDescription($value['description'])
        ->setBgColor($value['bg_color'])
        ->setCol($value['col'])
        ->setRow($value['row'])
        ->setAuthor($author)
        ->setStory($story);
      array_push($cards, $card);
    }

    return $cards;
  }

  public function insertCard(IndexCard $card) {
    $author_id = $card->getAuthor() ? $card->getAuthor()->getId() : null;

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->insert('IndexCard')
      ->values(
        array(
          'title' => '?',
          'description' => '?',
          'bg_color' => '?',
          'col' => '?',
          'row' => '?',
          'author_id' => '?',
          'story_id' => '?'
        ))
      ->setParameter(0, $card->getTitle())
      ->setParameter(1, $card->getDescription())
      ->setParameter(2, $card->getBgColor())
      ->setParameter(3, $card->getCol())
      ->setParameter(4, $card->getRow())
      ->setParameter(5, $author_id)
      ->setParameter(6, $card->getStory()->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting IndexCard", 500);
    }

    $card->setId($this->connection->lastInsertId('IndexCard'));

    return $card;
  }

  public function updateCard(IndexCard $card) {
    $author_id = $card->getAuthor() ? $card->getAuthor()->getId() : null;

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->update('IndexCard')
      ->set('title', '?')
      ->set('description', '?')
      ->set('bg_color', '?')
      ->set('col', '?')
      ->set('row', '?')
      ->set('author_id', '?')
      ->set('story_id', '?')
      ->where('card_id = ?')
      ->setParameter(0, $card->getTitle())
      ->setParameter(1, $card->getDescription())
      ->setParameter(2, $card->getBgColor())
      ->setParameter(3, $card->getCol())
      ->setParameter(4, $card->getRow())
      ->setParameter(5, $author_id)
      ->setParameter(6, $card->getStory()->getId())
      ->setParameter(7, $card->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Updating IndexCard", 500);
    }

    return $card;
  }

  public function deleteCard(IndexCard $card) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('IndexCard')
      ->where('card_id = ?')
      ->setParameter(0, $card->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      return false;
    }

    return true;
  }
}