<?php

namespace Ta1ler\Storymap\DataMapper;

use Doctrine\DBAL\Connection;

use Ta1ler\Storymap\Entity\Story;
use Ta1ler\Storymap\Entity\User;

class StoryMapper {

  private $connection;

  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  public function findStoryById($id) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'story_id',
        'title',
        'map_width',
        'map_height')
      ->from('Story')
      ->where('story_id = ?')
      ->setParameter(0, $id);
    $result = $queryBuilder->execute()->fetch();

    if ($result === false) {
      return;
    }

    $story = new Story();
    $story
      ->setId($result['story_id'])
      ->setTitle($result['title'])
      ->setMapWidth($result['map_width'])
      ->setMapHeight($result['map_height']);

    return $story;
  }

  public function findStoriesByUserId($userId) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        's.story_id',
        's.title',
        's.map_width',
        's.map_height')
      ->from('Story', 's')
      ->innerJoin('s', 'ProfileStory', 'sp', 's.story_id = sp.story_id')
      ->where('sp.user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute()->fetchAll();

    if ($result === false) {
      return;
    }

    $stories = array();

    foreach ($result as $value) {
      $story = new Story();
      $story
        ->setId($value['story_id'])
        ->setTitle($value['title'])
        ->setMapWidth($value['map_width'])
        ->setMapHeight($value['map_height']);
      array_push($stories, $story);
    }

    return $stories;
  }

  public function isOwner($story, $userId) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'user_id',
        'story_id')
      ->from('ProfileStory')
      ->where(
        $queryBuilder->expr()->andX(
          $queryBuilder->expr()->eq('user_id', '?'),
          $queryBuilder->expr()->eq('story_id', '?')
      ))
      ->setParameter(0, $userId)
      ->setParameter(1, $story->getId());
    $result = $queryBuilder->execute()->fetchAll();

    if ($result === false) {
      return false;
    }

    return true;
  }

  public function insertStory(Story $story, $userId) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->insert('Story')
      ->values(
        array(
          'title' => '?',
          'map_width' => '?',
          'map_height' => '?'
        ))
      ->setParameter(0, $story->getTitle())
      ->setParameter(1, $story->getMapWidth())
      ->setParameter(2, $story->getMapHeight());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting Story", 500);
    }

    $story->setId($this->connection->lastInsertId('Story'));

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->insert('ProfileStory')
      ->values(
        array(
          'story_id' => '?',
          'user_id' => '?'
        ))
      ->setParameter(0, $story->getId())
      ->setParameter(1, $userId);
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting ProfileStory", 500);
    }

    return $story;
  }

  public function updateUsers(Story $story, array $users) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('ProfileStory')
      ->where('story_id = ?')
      ->setParameter(0, $story->getId());
    $result = $queryBuilder->execute();

    foreach ($users as $user) {
      $queryBuilder = $this->connection->createQueryBuilder();
      $queryBuilder
        ->insert('ProfileStory')
        ->values(
          array(
            'story_id' => '?',
            'user_id' => '?'
          ))
        ->setParameter(0, $story->getId())
        ->setParameter(1, $user->getId());
      $result = $queryBuilder->execute();

      if ($result < 1) {
        throw new \Exception("Error Inserting ProfileStory", 500);
      }
    }

    return $story;
  }

  public function updateStory(Story $story) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->update('Story')
      ->set('title', '?')
      ->set('map_width', '?')
      ->set('map_height', '?')
      ->where('story_id', '?')
      ->setParameter(0, $story->getTitle())
      ->setParameter(1, $story->getMapWidth())
      ->setParameter(2, $story->getMapHeight())
      ->setParameter(3, $story->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Updating Story", 500);
    }

    return $story;
  }

  public function deleteStory(Story $story) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('ProfileStory')
      ->where('story_id = ?')
      ->setParameter(0, $story->getId());
    $result = $queryBuilder->execute();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('Story')
      ->where('story_id = ?')
      ->setParameter(0, $story->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      return false;
    }

    return true;
  }
}