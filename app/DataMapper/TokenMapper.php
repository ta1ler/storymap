<?php

namespace Ta1ler\Storymap\DataMapper;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;

use Ta1ler\Storymap\Entity\Token;
use Ta1ler\Storymap\Entity\User;
use Ta1ler\Storymap\DataMapper\UserMapper;

class TokenMapper {

  private $connection;
  private $userMapper;

  public function __construct(Connection $connection, UserMapper $userMapper) {
    $this->connection = $connection;
    $this->userMapper = $userMapper;
  }

  public function findTokenByName($name) {
    $token = new Token();
    $token
      ->setToken($name)
      ->splitToken();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select('token_id', 'user_id', 'validator', 'expiration_time')
      ->from('Token')
      ->where('selector = ?')
      ->setParameter(0, $token->getSelector());
    $result = $queryBuilder->execute()->fetch();

    if ($result === false || $result['validator'] != $token->getValidator()) {
      return;
    }

    $user = $this->userMapper->findUserById($result['user_id']);

    $token
      ->setId($result['token_id'])
      ->setExpirationTime(new \DateTime($result['expiration_time']))
      ->setUser($user);

    return $token;
  }

  public function insertToken(Token $token) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->insert('Token')
      ->values(
        array(
          'user_id' => '?',
          'selector' => '?',
          'validator' => '?',
          'expiration_time' => '?'
        ))
      ->setParameter(0, $token->getUser()->getId())
      ->setParameter(1, $token->getSelector())
      ->setParameter(2, $token->getValidator())
      ->setParameter(3, $token->getExpirationTime()->format('Y-m-d H:i:s'));
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting Token", 500);
    }

    return $token->setId($this->connection->lastInsertId('Token'));
  }

  public function deleteToken(Token $token) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('Token')
      ->where('token_id = ?')
      ->setParameter(0, $token->getId());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      return false;
    }

    return true;
  }
}