<?php

namespace Ta1ler\Storymap\DataMapper;

use Doctrine\DBAL\Connection;

use Ta1ler\Storymap\Entity\User;

class UserMapper {

  private $connection;

  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  public function findUserById($id) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'u.user_id',
        'u.first_name',
        'u.second_name',
        'la.hashed_pwd',
        'la.email',
        'av.filepath')
      ->from('Profile', 'u')
      ->innerJoin('u', 'LocalAuth', 'la', 'u.user_id = la.user_id')
      ->leftJoin('u', 'Avatar', 'av', 'u.avatar_id = av.avatar_id')
      ->where('u.user_id = ?')
      ->setParameter(0, $id);
    $result = $queryBuilder->execute()->fetch();

    if ($result === false) {
      return;
    }

    $user = new User();
    $user
      ->setId($id)
      ->setFirstName($result['first_name'])
      ->setSecondName($result['second_name'])
      ->setHashedPwd($result['hashed_pwd'])
      ->setEmail($result['email'])
      ->setAvatarPath($result['filepath']);

    return $user;
  }

  public function findUserByEmail($email) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'u.user_id',
        'u.first_name',
        'u.second_name',
        'la.hashed_pwd',
        'la.email',
        'av.filepath')
      ->from('Profile', 'u')
      ->innerJoin('u', 'LocalAuth', 'la', 'u.user_id = la.user_id')
      ->leftJoin('u', 'Avatar', 'av', 'u.avatar_id = av.avatar_id')
      ->where('la.email = ?')
      ->setParameter(0, $email);
    $result = $queryBuilder->execute()->fetch();

    if ($result === false) {
      return;
    }

    $user = new User();
    $user
      ->setId($result['user_id'])
      ->setFirstName($result['first_name'])
      ->setSecondName($result['second_name'])
      ->setHashedPwd($result['hashed_pwd'])
      ->setEmail($email)
      ->setAvatarPath($result['filepath']);

    return $user;
  }

  public function findUsersByStory($storyId) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'u.user_id',
        'u.first_name',
        'u.second_name',
        'la.hashed_pwd',
        'la.email',
        'av.filepath')
      ->from('Profile', 'u')
      ->innerJoin('u', 'LocalAuth', 'la', 'u.user_id = la.user_id')
      ->leftJoin('u', 'Avatar', 'av', 'u.avatar_id = av.avatar_id')
      ->innerJoin('u', 'ProfileStory', 'ps', 'u.user_id = ps.user_id')
      ->where('ps.story_id = ?')
      ->setParameter(0, $storyId);
    $result = $queryBuilder->execute()->fetchAll();

    if ($result === false) {
      return;
    }

    $users = array();

    foreach ($result as $value) {
      $user = new User();
      $user
        ->setId($value['user_id'])
        ->setFirstName($value['first_name'])
        ->setSecondName($value['second_name'])
        ->setHashedPwd($value['hashed_pwd'])
        ->setEmail($value['email'])
        ->setAvatarPath($value['filepath']);
      array_push($users, $user);
    }

    return $users;
  }

  public function isEmailUsed($email) {
    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select(
        'u.user_id',
        'la.email')
      ->from('Profile', 'u')
      ->innerJoin('u', 'LocalAuth', 'la', 'u.user_id = la.user_id')
      ->where('la.email = ?')
      ->setParameter(0, $email);
    $result = $queryBuilder->execute()->fetch();

    if ($result === false) {
      return false;
    }

    return true;
  }

  public function insertUser(User $user) {
    $avatarId = null;

    if (!empty($user->getAvatarPath())) {
      $queryBuilder = $this->connection->createQueryBuilder();

      $queryBuilder
        ->insert('Avatar')
        ->values(
          array(
            'filepath' => '?'
          ))
        ->setParameter(0, $user->getAvatarPath());
      $result = $queryBuilder->execute();

      if ($result < 1) {
        throw new \Exception("Error Inserting Avatar", 500);
      }

      $avatarId = $this->connection->lastInsertId('Avatar');
    }

    $queryBuilder = $this->connection->createQueryBuilder();

    $queryBuilder
      ->insert('Profile')
      ->values(
        array(
          'first_name' => '?',
          'second_name' => '?',
          'avatar_id' => '?'
        ))
      ->setParameter(0, $user->getFirstName())
      ->setParameter(1, $user->getSecondName())
      ->setParameter(2, $avatarId);
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting User", 500);
    }

    $user->setId($this->connection->lastInsertId('Profile'));

    $queryBuilder = $this->connection->createQueryBuilder();

    $queryBuilder
      ->insert('LocalAuth')
      ->values(
        array(
          'user_id' => '?',
          'hashed_pwd' => '?',
          'email' => '?'
        ))
      ->setParameter(0, $user->getId())
      ->setParameter(1, $user->getHashedPwd())
      ->setParameter(2, $user->getEmail());
    $result = $queryBuilder->execute();

    if ($result < 1) {
      throw new \Exception("Error Inserting LocalAuth", 500);
    }

    return $user;
  }

  public function deleteUser(User $user) {
    $userId = $user->getId();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('LocalAuth')
      ->where('user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('ProfileStory')
      ->where('user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->select('avatar_id')
      ->from('Profile')
      ->where('user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute()->fetch();

    if ($result !== false) {
      $avatar_id = $result['avatar_id'];

      $queryBuilder = $this->connection->createQueryBuilder();
      $queryBuilder
        ->delete('Avatar')
        ->where('avatar_id = ?')
        ->setParameter(0, $avatar_id);
      $result = $queryBuilder->execute();
    }

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->update('IndexCard')
      ->set('author_id', '?')
      ->where('author_id = ?')
      ->setParameter(0, null)
      ->setParameter(1, $userId);
    $result = $queryBuilder->execute();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('Token')
      ->where('user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute();

    $queryBuilder = $this->connection->createQueryBuilder();
    $queryBuilder
      ->delete('Profile')
      ->where('user_id = ?')
      ->setParameter(0, $userId);
    $result = $queryBuilder->execute();

    if ($result < 1) {
      return false;
    }

    return true;
  }
}