<?php

namespace Ta1ler\Storymap\Entity;

class IndexCard
{
  private $id;
  private $title;
  private $description;
  private $bgColor;
  private $col;
  private $row;
  private $author;
  private $story;

  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setDescription($description = null)
  {
    $this->description = $description;

    return $this;
  }

  public function getDescription()
  {
    return $this->description;
  }

  public function setBgColor($bgColor = null)
  {
    $this->bgColor = $bgColor;

    return $this;
  }

  public function getBgColor()
  {
    return $this->bgColor;
  }

  public function setCol($col)
  {
    $this->col = $col;

    return $this;
  }

  public function getCol()
  {
    return $this->col;
  }

  public function setRow($row)
  {
    $this->row = $row;

    return $this;
  }

  public function getRow()
  {
    return $this->row;
  }

  public function setAuthor(User $author = null)
  {
    if ($author) {
      $author = clone $author;
    }

    $this->author = $author;

    return $this;
  }

  public function getAuthor()
  {
    return $this->author;
  }

  public function setStory(Story $story = null)
  {
    if ($story) {
      $story = clone $story;
    }

    $this->story = $story;

    return $this;
  }

  public function getStory()
  {
    return $this->story;
  }
}
