<?php

namespace Ta1ler\Storymap\Entity;

class Story
{
  private $id;
  private $title;
  private $mapWidth = 0;
  private $mapHeight = 0;

  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setMapWidth($mapWidth)
  {
    $this->mapWidth = $mapWidth;

    return $this;
  }

  public function getMapWidth()
  {
    return $this->mapWidth;
  }

  public function setMapHeight($mapHeight)
  {
    $this->mapHeight = $mapHeight;

    return $this;
  }

  public function getMapHeight()
  {
    return $this->mapHeight;
  }
}
