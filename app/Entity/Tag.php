<?php

namespace Ta1ler\Entity;

/**
 * Tag
 */
class Tag
{
  /**
   * @var int
   */
  private $tagId;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $color;

  /**
   * @var \Story
   */
  private $story;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $card;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->card = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get tagId.
   *
   * @return int
   */
  public function getTagId()
  {
    return $this->tagId;
  }

  /**
   * Set name.
   *
   * @param string $name
   *
   * @return Tag
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * Get name.
   *
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set color.
   *
   * @param string $color
   *
   * @return Tag
   */
  public function setColor($color)
  {
    $this->color = $color;

    return $this;
  }

  /**
   * Get color.
   *
   * @return string
   */
  public function getColor()
  {
    return $this->color;
  }

  /**
   * Set story.
   *
   * @param \Story|null $story
   *
   * @return Tag
   */
  public function setStory(\Story $story = null)
  {
    $this->story = $story;

    return $this;
  }

  /**
   * Get story.
   *
   * @return \Story|null
   */
  public function getStory()
  {
    return $this->story;
  }

  /**
   * Add card.
   *
   * @param \Indexcard $card
   *
   * @return Tag
   */
  public function addCard(\Indexcard $card)
  {
    $this->card[] = $card;

    return $this;
  }

  /**
   * Remove card.
   *
   * @param \Indexcard $card
   *
   * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
   */
  public function removeCard(\Indexcard $card)
  {
    return $this->card->removeElement($card);
  }

  /**
   * Get card.
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCard()
  {
    return $this->card;
  }
}
