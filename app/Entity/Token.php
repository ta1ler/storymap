<?php

namespace Ta1ler\Storymap\Entity;

class Token
{
  private $id;
  private $token;
  private $selector;
  private $validator;
  private $expirationTime;
  private $user;

  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setToken($token)
  {
    $this->token = $token;

    return $this;
  }

  public function getToken()
  {
    return $this->token;
  }

  public function splitToken() {
    $this->selector = substr($this->token, 0, 20);
    $this->validator = sha1(substr($this->token, 20, 20));

    return $this;
  }

  public function getSelector()
  {
    return $this->selector;
  }

  public function getValidator()
  {
    return $this->validator;
  }

  public function setExpirationTime(\DateTime $expirationTime)
  {
    $this->expirationTime = $expirationTime;

    return $this;
  }

  public function getExpirationTime()
  {
    return $this->expirationTime;
  }

  public function setUser(User $user = null)
  {
    if ($user) {
      $user = clone $user;
    }

    $this->user = $user;

    return $this;
  }

  public function getUser()
  {
    return $this->user;
  }

  public function isExpired() {
    $now = new \DateTime();

    return ($now > $this->getExpirationTime());
  }
}
