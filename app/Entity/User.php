<?php

namespace Ta1ler\Storymap\Entity;

class User
{
  private $id;
  private $firstName;
  private $secondName;
  private $hashedPwd;
  private $email;
  private $avatarPath;

  public function setId($id)
  {
    $this->id = $id;

    return $this;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;

    return $this;
  }

  public function getFirstName()
  {
    return $this->firstName;
  }

  public function setSecondName($secondName)
  {
    $this->secondName = $secondName;

    return $this;
  }

  public function getSecondName()
  {
    return $this->secondName;
  }

  public function setHashedPwd($hashedPwd)
  {
    $this->hashedPwd = $hashedPwd;

    return $this;
  }

  public function getHashedPwd()
  {
    return $this->hashedPwd;
  }

  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setAvatarPath($avatarPath)
  {
    $this->avatarPath = $avatarPath;

    return $this;
  }

  public function getAvatarPath()
  {
    return $this->avatarPath;
  }

  public function matchPassword($password) {
    return password_verify($password, $this->getHashedPwd());
  }
}
