<?php

namespace Ta1ler\Storymap\Service;

use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception;

class RequestHandler
{
    protected $matcher;
    protected $controllerResolver;
    protected $argumentResolver;
    protected $requestStack;

    public function __construct(
        UrlMatcher $urlMatcher,
        ContainerControllerResolver $controllerResolver,
        ArgumentResolver $argumentResolver,
        RequestStack $requestStack
    )
    {
        $this->matcher = $urlMatcher;
        $this->controllerResolver = $controllerResolver;
        $this->argumentResolver = $argumentResolver;
        $this->requestStack = $requestStack;
    }

    public function handle()
    {
        $request = $this->requestStack->getCurrentRequest();

        $this->matcher->getContext()->fromRequest($request);

        try {
            $request->attributes->add($this->matcher->match($request->getPathInfo()));

            $controller = $this->controllerResolver->getController($request);
            $arguments = $this->argumentResolver->getArguments($request, $controller);

            return call_user_func_array($controller, $arguments);
        } catch (Exception\ResourceNotFoundException $e) {
            return new Response('Bad Request', 400);
        } catch (Exception\MethodNotAllowedException $e) {
            return new Response('Method Not Allowed', 405);
        } catch (Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }
}