<?php

namespace Ta1ler\Storymap\Service;

use Ta1ler\Storymap\Entity\Token;
use Ta1ler\Storymap\Entity\User;
use Ta1ler\Storymap\DataMapper\TokenMapper;
use Ta1ler\Storymap\DataMapper\UserMapper;

class SessionManager {

  private $tokenMapper;
  private $userMapper;

  public function __construct(TokenMapper $tokenMapper, UserMapper $userMapper) {
    $this->tokenMapper = $tokenMapper;
    $this->userMapper = $userMapper;
  }

  public function loginWithPassword($email, $password) {
    $user = $this->userMapper->findUserByEmail($email);

    if (is_null($user) || ($user->matchPassword($password) === false)) {
      return;
    }

    $token = $this->createToken($user);
    $token = $this->tokenMapper->insertToken($token);

    return $token;
  }

  public function loginWithToken($tokenString) {
    if (!($token = $this->tokenMapper->findTokenByName($tokenString))) {
      return;
    }

    if ($token->isExpired()) {
      $this->tokenMapper->deleteToken($token);
      return;
    }

    return $token;
  }

  public function registerUser($email, $password, $firstName, $secondName, $avatarPath) {
    if (!$this->isValidEmail($email)) {
      return;
    }
    if (!$this->isValidPassword($password)) {
      return;
    }
    if (!$this->isValidName($firstName)) {
      return;
    }
    if (!$this->isValidName($secondName)) {
      return;
    }
    if (!$this->isValidPath($avatarPath)) {
      return;
    }
    if ($this->userMapper->isEmailUsed($email)) {
      return;
    }

    $user = $this->createUser($email, $password, $firstName, $secondName, $avatarPath);
    $user = $this->userMapper->insertUser($user);

    $token = $this->createToken($user);
    $token = $this->tokenMapper->insertToken($token);

    $data = [
      'token' => $token,
      'user' => $user
    ];

    return $data;
  }

  public function isAuthorized(User $user, Token $token) {
    return ($user->getId() === $token->getUser()->getId());
  }

  public function getUser($userId) {
    if (!($user = $this->userMapper->findUserById($userId))) {
      return;
    }

    return $user;
  }

  public function deleteUser(User $user, Token $token) {
    if (!$this->isAuthorized($user, $token)) {
      return false;
    }

    return $this->userMapper->deleteUser($user);
  }

  public function updateToken(Token $token) {
    $user = $token->getUser();
    $this->tokenMapper->deleteToken($token);
    $token = $this->createToken($user);
    $token = $this->tokenMapper->insertToken($token);

    return $token;
  }

  public function deleteToken(Token $token) {
    return $this->tokenMapper->deleteToken($token);
  }

  private function createToken(User $user) {
    $tokenString = sha1(openssl_random_pseudo_bytes(1024));
    $datetime = new \DateTime();
    $datetime->modify('+1 day');

    $token = new Token();
    $token
      ->setToken($tokenString)
      ->splitToken()
      ->setExpirationTime($datetime)
      ->setUser($user);

    return $token;
  }

  private function createUser($email, $password, $firstName, $secondName, $avatarPath) {
    $hashedPwd = password_hash($password, PASSWORD_DEFAULT);

    $user = new User();
    $user
      ->setEmail($email)
      ->setHashedPwd($hashedPwd)
      ->setFirstName($firstName)
      ->setSecondName($secondName)
      ->setAvatarPath($avatarPath);

    return $user;
  }

  // MOCK
  private function isValidEmail($email) {
    return true;
  }

  // MOCK
  private function isValidPassword($password) {
    return true;
  }

  // MOCK
  private function isValidName($name) {
    return true;
  }

  // MOCK
  private function isValidPath($path) {
    return true;
  }
}