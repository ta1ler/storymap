<?php

namespace Ta1ler\Storymap\Service;

use Ta1ler\Storymap\Service\SessionManager;
use Ta1ler\Storymap\DataMapper\UserMapper;
use Ta1ler\Storymap\DataMapper\StoryMapper;
use Ta1ler\Storymap\DataMapper\CardMapper;
use Ta1ler\Storymap\Entity\Token;
use Ta1ler\Storymap\Entity\User;
use Ta1ler\Storymap\Entity\Story;
use Ta1ler\Storymap\Entity\IndexCard;

class StoryManager {
  private $sessionManager;
  private $storyMapper;
  private $userMapper;
  private $cardMapper;

  public function __construct(SessionManager $sessionManager, UserMapper $userMapper, StoryMapper $storyMapper, CardMapper $cardMapper) {
    $this->sessionManager = $sessionManager;
    $this->userMapper = $userMapper;
    $this->storyMapper = $storyMapper;
    $this->cardMapper = $cardMapper;
  }

  public function getUserStories(User $user, Token $token) {
    if (!$this->sessionManager->isAuthorized($user, $token)) {
      return;
    }

    return $this->storyMapper->findStoriesByUserId($user->getId());
  }

  public function getUsers(Story $story) {
    return $this->userMapper->findUsersByStory($story->getId());
  }

  public function getStory($storyId, Token $token) {
    $story = $this->storyMapper->findStoryById($storyId);

    if (is_null($story)) {
      return;
    }

    if (!$this->storyMapper->isOwner($story, $token->getUser()->getId())) {
      return;
    }

    return $story;
  }

  public function getStoryCards(Story $story) {
    return $this->cardMapper->findCardsByStory($story->getId());
  }

  public function getCard($cardId, User $user) {
    $card = $this->cardMapper->findCardById($cardId);

    if (is_null($card)) {
      return;
    }

    if (!$this->storyMapper->isOwner($card->getStory(), $user->getId())) {
      return;
    }

    return $card;
  }

  public function changeUsers(Story $story, array $users) {
    $story = $this->storyMapper->updateUsers($story, $users);

    if (is_null($story)) {
      return;
    }

    return $story;
  }

  public function deleteStory($storyId, Token $token) {
    $story = $this->storyMapper->findStoryById($storyId);

    if (is_null($story)) {
      return;
    }

    if (!$this->storyMapper->isOwner($story, $token->getUser()->getId())) {
      return;
    }

    return $this->storyMapper->deleteStory($story);
  }

  public function deleteCard($cardId, User $user) {
    $card = $this->cardMapper->findCardById($cardId);

    if (is_null($card)) {
      return false;
    }

    if (!$this->storyMapper->isOwner($card->getStory(), $user->getId())) {
      return false;
    }

    return $this->cardMapper->deleteCard($card);
  }

  public function createStory($title, User $user, Token $token) {
    if (!$this->sessionManager->isAuthorized($user, $token)) {
      return;
    }

    if (!$this->isValidTitle($title)) {
      return;
    }

    $story = new Story();
    $story->setTitle($title);

    $story = $this->storyMapper->insertStory($story, $user->getId());

    return $story;
  }

  public function createCard($title, $description, $bgColor, $col, $row, Story $story, User $user) {
    if (!$this->isValidTitle($title)) {
      return;
    }

    if (!$this->isValidDescription($description)) {
      return;
    }

    if (!$this->isValidColor($bgColor)) {
      return;
    }

    if (!$this->isValidPosition($col, $row, $story)) {
      return;
    }

    $card = new IndexCard();
    $card
      ->setTitle($title)
      ->setDescription($description)
      ->setBgColor($bgColor)
      ->setCol($col)
      ->setRow($row)
      ->setAuthor($user)
      ->setStory($story);

    $card = $this->cardMapper->insertCard($card);

    $this->updateStorymapSize($story);

    return $card;
  }

  private function updateStorymapSize(Story $story) {
    $cards = $this->getStoryCards($story);

    $maxCol = -1;
    $maxRow = -1;

    foreach ($cards as $card) {
      if ($maxCol < $card->getCol()) {
        $maxCol = $card->getCol();
      }

      if ($maxRow < $card->getRow()) {
        $maxRow = $card->getRow();
      }
    }

    if ($story->getMapWidth() > $maxCol && $story->getMapHeight() > $maxRow) {
      return;
    }

    $story->setMapWidth($maxCol + 1);
    $story->setMapHeight($maxRow + 1);

    $this->storyMapper->updateStory($story);
  }

  public function changeCard(IndexCard $card, $title, $description, $bgColor, $col, $row) {
    if (!$this->isValidTitle($title)) {
      return;
    }

    if (!$this->isValidDescription($description)) {
      return;
    }

    if (!$this->isValidColor($bgColor)) {
      return;
    }

    if (!$this->isValidPosition($col, $row, $card->getStory())) {
      return;
    }

    $card
      ->setTitle($title)
      ->setDescription($description)
      ->setBgColor($bgColor)
      ->setCol($col)
      ->setRow($row);

    $card = $this->cardMapper->updateCard($card);

    $this->updateStorymapSize($card->getStory());

    return $card;
  }

  // MOCK
  private function isValidTitle($title) {
    return true;
  }

  // MOCK
  private function isValidDescription($description) {
    return true;
  }

  // MOCK
  private function isValidColor($color) {
    return true;
  }

  // MOCK
  private function isValidPosition($col, $row, Story $story) {
    return true;
  }
}