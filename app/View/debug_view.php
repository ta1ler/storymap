<div id="apirequest">
  <form id="request">
    <label for="url">URL</label><br>
    <input type="text" name="url" id="url"><br>
    <label for="method">METHOD</label><br>
    <select id="method" name="method">
      <option value="head">HEAD</option>
      <option value="get">GET</option>
      <option value="post">POST</option>
      <option value="put">PUT</option>
      <option value="patch">PATCH</option>
      <option value="delete">DELETE</option>
    </select><br>
    <label for="auth">AUTHORIZATION</label><br>
    <select name="auth" id="auth">
      <option value="">NONE</option>
      <option value="Basic">BASIC</option>
      <option value="Bearer">TOKEN</option>
    </select><br>
    <label>USER/PASSWORD</label><br>
    <input type="text" name="user" id="user"><input type="password" name="pwd" id="pwd"><br>
    <label for="token">TOKEN</label><br>
    <input type="text" name="token" id="token"><br>
    <label for="data">DATA (JSON)</label><br>
    <textarea name="data" id="data"></textarea><br>
    <button name="submit">Submit</button>  
  </form>
</div>
<div id="apiresponse">
	<p>RESPONSE</p>
	<p id="response-code"></p>
	<p>RESPONSE DATA</p>
	<p id="response-data"></p>
</div>