<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Storymap Tool</title>

    <!-- Required Stylesheets -->
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css"/>

    <!-- Required scripts -->
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
    <script src="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>

    <!-- Axios -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  </head>

  <body>
    <!-- Our application root element -->
    <div id="app">
      <b-navbar toggleable="md" type="dark" variant="primary">
        <b-navbar-toggle target="nav_collapse"></b-navbar-toggle>
        <b-navbar-brand href="#">STORYMAP</b-navbar-brand>

        <b-collapse is-nav id="nav_collapse">
          <!-- Right aligned nav items -->
          <b-navbar-nav class="ml-auto">
            <b-nav-item-dropdown text="Stories" right v-if="authorized">
            </b-nav-item-dropdown>

            <b-nav-item v-b-modal.signin v-if="!authorized">Sign In</b-nav-item>
            <b-nav-item v-b-modal.signup v-if="!authorized">Sign Up</b-nav-item>

            <b-nav-item-dropdown right v-if="authorized">
              <!-- Using button-content slot -->
              <template slot="button-content">
                <em>{{this.user.profile.firstName + ' ' + this.user.profile.secondName}}</em>
              </template>
              <b-dropdown-item href="#">Profile</b-dropdown-item>
              <b-dropdown-item href="#">Signout</b-dropdown-item>
            </b-nav-item-dropdown>
          </b-navbar-nav>
        </b-collapse>
      </b-navbar>

      <b-modal id="signin"
               title="Sign In"
               ok-title="Submit"
               @ok="handleSignin"
               @shown="clearSignin">
        <div>
          <b-form @submit="onSubmit" @reset="onReset">
            <b-form-group id="emailSigninLabel"
                          label="Email address:"
                          label-for="emailSigninInput">
              <b-form-input id="emailSigninInput"
                            type="email"
                            v-model="signin.email"
                            required
                            placeholder="Enter email">
              </b-form-input>
            </b-form-group>
            <b-form-group id="passwordSigninLabel"
                          label="Password:"
                          label-for="passwordSigninInput">
              <b-form-input id="passwordSigninInput"
                            type="password"
                            v-model="signin.password"
                            required
                            placeholder="Enter password">
              </b-form-input>
            </b-form-group>
          </b-form>
        </div>
      </b-modal>

      <b-modal id="signup"
               ref="signupModal"
               title="Sign Up"
               ok-title="Submit"
               @ok="handleSignup"
               @shown="clearSignup">
        <div>
          <b-alert variant="danger"
                   dismissible
                   :show="signup.alert"
                   @dismissed="signup.alert=false">
            Invalid Data!
          </b-alert>
          <b-alert variant="danger"
                   dismissible
                   :show="signup.existAlert"
                   @dismissed="signup.existAlert=false">
            A user with the same e-mail is already exists!
          </b-alert>
          <b-form>
            <b-form-group id="firstNameSignupLabel"
                          label="First name:"
                          label-for="firstNameSignupInput">
              <b-form-input id="firstNameSignupInput"
                            type="text"
                            v-model="signup.firstName"
                            required
                            placeholder="Enter first name"
                            :state="signupFirstNameState">
              </b-form-input>
            </b-form-group>
            <b-form-group id="secondNameSignupLabel"
                          label="Second name:"
                          label-for="firstNameSignupInput">
              <b-form-input id="secondNameSignupInput"
                            type="text"
                            v-model="signup.secondName"
                            required
                            placeholder="Enter second name"
                            :state="signupSecondNameState">
              </b-form-input>
            </b-form-group>
            <b-form-group id="emailSignupLabel"
                          label="Email address:"
                          label-for="emailSignupInput">
              <b-form-input id="emailSignupInput"
                            type="email"
                            v-model="signup.email"
                            required
                            placeholder="Enter email"
                            :state="signupEmailState">
              </b-form-input>
            </b-form-group>
            <b-form-group id="passwordSignupLabel"
                          label="Password:"
                          label-for="passwordSignupInput">
              <b-form-input id="passwordSignupInput"
                            type="password"
                            v-model="signup.password"
                            required
                            placeholder="Enter password"
                            :state="signupPasswordState">
              </b-form-input>
            </b-form-group>
            <b-form-group id="confirmPasswordSignupLabel"
                          label="Confirm password:"
                          label-for="confirmPasswordSignupInput">
              <b-form-input id="confirmPasswordSignupInput"
                            type="password"
                            v-model="signup.confirmPassword"
                            required
                            placeholder="Confirm password"
                            :state="signupConfirmPasswordState">
              </b-form-input>
            </b-form-group>
          </b-form>
        </div>
      </b-modal>

      <!-- <b-container>
        <b-jumbotron header="Bootstrap Vue"
                     lead="Bootstrap 4 Components for Vue.js 2"
        >
          <p>For more information visit our website</p>
          <b-btn variant="primary" href="https://bootstrap-vue.js.org/">More Info</b-btn>
        </b-jumbotron>

        <b-form-group horizontal
                      :label-cols="4"
                      description="Let us know your name."
                      label="Enter your name"
        >
           <b-form-input v-model.trim="name"></b-form-input>
        </b-form-group>

        <b-alert variant="success" :show="showAlert">
          Hello {{ name }}
        </b-alert>
      </b-container> -->
    </div>

    <script src="static/js/main.js"></script>
  </body>
</html>