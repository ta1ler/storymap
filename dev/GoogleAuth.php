<?php

class GoogleAuth {
  const GRANT_TYPE = 'authorization_code';

  private $authorization_endpoint;
  private $token_endpoint;
  private $token_info_endpoint;
  private $jwks_uri;

  private $client_id;
  private $client_secret;

  public function __construct($openid_config_url, $client_config) {
    // Get openid config from Google url
    // Current: https://accounts.google.com/.well-known/openid-configuration
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $openid_config_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $openid_config = json_decode(curl_exec($ch), true);

    curl_close($ch);

    $this->authorization_endpoint = $openid_config['authorization_endpoint'];
    $this->token_endpoint = $openid_config['token_endpoint'];
    $this->token_info_endpoint = "https://www.googleapis.com/oauth2/v3/tokeninfo"; // No endpoint in config :|
    $this->jwks_uri = $openid_config['jwks_uri'];

    // Get client (application) config
    $this->client_id = $client_config['client_id'];
    $this->client_secret = $client_config['client_secret'];
  }

  public function signIn($options) {
    $query = array(
      'redirect_uri'  => $options['redirect_uri'],
      'response_type' => $options['response_type'],
      'client_id'     => $this->client_id,
      'scope'         => $options['scope']
    );
    $url = $this->authorization_endpoint . '?' . http_build_query($query);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_exec($ch);

    curl_close($ch);
  }

  public function getToken($options) {
    $post_body_arr = array(
      'code'          => $options['code'],
      'redirect_uri'  => $options['redirect_uri'],
      'client_id'     => $this->client_id,
      'client_secret' => $this->client_secret,
      'grant_type'    => self::GRANT_TYPE
    );
    $post_body = http_build_query($post_body_arr);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $this->token_endpoint);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);

    $response = json_decode(curl_exec($ch), true);

    curl_close($ch);

    return $response;
  }

  public function getIdentity($jwt, $is_debug) {
    if ($is_debug) {
      $query = array(
        'id_token' => $jwt
      );
      $url = $this->token_info_endpoint . '?' . http_build_query($query);

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $response = json_decode(curl_exec($ch), true);

      curl_close($ch);

      return $response;
    } else {
      return;
    }
  }
}

$auth = new GoogleAuth("https://accounts.google.com/.well-known/openid-configuration", [
  'client_id'     => "",
  'client_secret' => ""
]);

if(empty($_GET)) {
  $auth->signIn([
    'redirect_uri'  => "",
    'response_type' => "code",
    'scope'         => "openid email"
  ]);
}

$token = $auth->getToken([
  'code' => $_GET['code'],
  'redirect_uri'  => ""
]);

$identity = $auth->getIdentity($token['id_token'], true);

var_dump($identity);