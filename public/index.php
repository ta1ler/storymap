<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection;
use Symfony\Component\HttpFoundation\Request;

use Ta1ler\Storymap\Service\RequestHandler;

// Getting FileLocator
$fileLocator = new FileLocator(array(__DIR__ . '/../config'));

// Dependency Injection
$container = new DependencyInjection\ContainerBuilder();
$loader = new DependencyInjection\Loader\YamlFileLoader($container, $fileLocator);
$loader->load('services.yaml');
$container->compile();

// var_dump(getallheaders());
// var_dump($container->get('Doctrine\DBAL\Connection'));
// var_dump($container->get('Ta1ler\Storymap\Controller\Controller'));

// Handling the request
$requestStack = $container->get('Symfony\Component\HttpFoundation\RequestStack');
$request = Request::createFromGlobals();
$requestStack->push($request);
$frame = $container->get('Ta1ler\Storymap\Service\RequestHandler');
// $frame = $container->get('Symfony\Component\HttpKernel\HttpKernel');
$frame->handle($request)->send();