jQuery(document).ready(function () {
  'use strict';

  // start();

  // function start() {

  // }

  jQuery('#request').submit(function (event) {
    event.preventDefault();

    jQuery('p#response-code').text('');
    jQuery('p#response-data').text('');

    var url = jQuery('input[name="url"]').val();
    var method = jQuery('select[name="method"]').val();
    var auth = jQuery('select[name="auth"]').val();
    var user = jQuery('input[name="user"]').val();
    var pwd = jQuery('input[name="pwd"]').val();
    var token = jQuery('input[name="token"]').val();
    var data = jQuery('textarea[name="data"]').val();
    // if (data !== null && data !== undefined) {
    //   data = 'data=' + data;
    // }

    console.log(url);
    console.log(method);
    console.log(auth);
    console.log(user);
    console.log(pwd);
    console.log(token);
    console.log(data);

    jQuery.ajax({
      url: url,
      type: method,
      data: data,
      beforeSend: function (xhr) {
        if (!auth) {
          return;
        }
        if (auth === 'Basic') {
          xhr.setRequestHeader("Authorization", auth + ' ' + btoa(user + ':' + pwd));
        } else {
          xhr.setRequestHeader("Authorization", auth + ' ' + token);
        }
      },
      success: function (data, textStatus, xhr) {
        console.log('Success');
        jQuery('p#response-code').text(xhr.status);
        jQuery('p#response-data').html('<div>' + xhr.responseText +'</div>');
      },
      error: function (xhr, textStatus, err) {
        console.log('Error');
        jQuery('p#response-code').text(xhr.status);
        jQuery('p#response-data').text(xhr.responseText);
      }
    });
  });
});