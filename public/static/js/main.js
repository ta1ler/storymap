'use strict';

// const axios = require('axios');

window.app = new Vue({
  el: "#app",
  data: {
    name: '', // DELETE
    signup: {
      firstName: '',
      secondName: '',
      email: '',
      password: '',
      confirmPassword: '',
      alert: false,
      existAlert: false
    },
    signin: {
      email: '',
      password: ''
    },
    show: true, // DELETE
    user: {
      token: '',
      profile: {
        id: 0,
        firstName: '',
        secondName: '',
        email: ''
      }
    },
    stories: []
  },
  computed: {
    authorized() {
      return (this.user.token);
    },

    signupFirstNameState() {
      return this.signup.firstName.length >= 1 ? true : false;
    },
    signupSecondNameState() {
      return this.signup.secondName.length >= 1 ? true : false;
    },
    signupEmailState() {
      return (this.signup.email.length >= 3 && this.signup.email.includes('@'));
    },
    signupPasswordState() {
      return this.signup.password.length >= 10 ? true : false;
    },
    signupConfirmPasswordState() {
      return (this.signup.confirmPassword.length > 0 && this.signup.confirmPassword === this.signup.password);
    },

    showAlert() { // DELETE
      return this.name.length > 4 ? true : false;
    }
  },

  mounted: function () {
    // this.user.token = getCookie('token');
    // console.log(this);
    // console.log(this.user);
    // console.log(this.user.profile);
  },

  methods: {
    getCookie (name) {
      match = document.cookie.match(new RegExp(name + '=([^;]+)'));
      if (match) return match[1];
      return;
    },

    handleSignup(evt) {
      evt.preventDefault();
      if (!this.signupFirstNameState
        || !this.signupSecondNameState
        || !this.signupEmailState
        || !this.signupPasswordState
        || !this.signupConfirmPasswordState
        ) {
        this.signup.alert = true;
        return false;
      }

      axios
        .post('/api/user/', {
          email: this.signup.email,
          firstName: this.signup.firstName,
          secondName: this.signup.secondName,
          password: this.signup.password
        })
        .then(response => {
          console.log(response);
          this.user.token = response.data.data.token;
          this.user.profile.id = response.data.data.user.id;
          this.user.profile.firstName = response.data.data.user.firstName;
          this.user.profile.secondName = response.data.data.user.secondName;
          this.user.profile.email = this.signup.email;
        })
        .catch(error => {
          console.log(error);
        });

      this.$refs.signupModal.hide();
    },

    clearSignup() {
      this.signup.firstName = '';
      this.signup.secondName = '';
      this.signup.email = '';
      this.signup.password = '';
      this.signup.confirmPassword = '';
      this.signup.alert = false;
      this.signup.existAlert = false;
    },

    onSubmit (evt) { // DELETE
      evt.preventDefault();
      alert(JSON.stringify(this.form));
    },

    onReset (evt) { // DELETE
      evt.preventDefault();
      /* Reset our form values */
      this.form.email = '';
      this.form.name = '';
      this.form.food = null;
      this.form.checked = [];
      /* Trick to reset/clear native browser form validation state */
      this.show = false;
      this.$nextTick(() => { this.show = true });
    }
  }
});