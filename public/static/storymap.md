# Story Map
## Data Model
### User management
Profile:
- user_id (FK)
- first_name
- second_name
- avatar_id (FK)

Token:
- token_id (PK)
- user_id (FK)
- selector_token
- validator_token
- expiration_time

User:
- user_id (PK)
- email
- hashed_pwd

Avatar:
- avatar_id (PK)
- filepath

### Storymap
Story
- story_id (PK)
- title
- map_width
- map_height

Story - User
- profile_id (PK, FK)
- story_id (PK, FK)

IndexCard
- card_id (PK)
- story_id (FK)
- title
- description
- col
- row
- bg_color
- author_id (FK)

IndexCard - Tag
- card_id (PK, FK)
- tag_id (PK, FK)

Tag
- tag_id (PK)
- story_id (FK)
- name
- color

## Pages
- landing page
  - header with profile component
  - app description section
  - footer
- workspace page
  - header with profile component
  - stories sidelist
    - users popup
  - story map section
  - index card popup
    - tags component
    - author tooltip
  - footer
- profile component
  - login popup
  - registration popup
  - user profile popup

## API Routes
### Users and authentication
| Method    | Route
|---
| GET       | api\\token
| DELETE    | api\\token
| GET       | api\\user\\{user_id}
| GET       | api\\user\\current
| POST      | api\\user
| DELETE    | api\\user\\{user_id}
| GET       | api\\user\\{user_id}\\stories

### Stories
| Method    | Route
|---
| GET       | api\\story\\{story_id}
| POST      | api\\story
| DELETE    | api\\story\\{story_id}
| GET       | api\\story\\{story_id}\\users
| POST      | api\\story\\{story_id}\\users
| GET       | api\\story\\{story_id}\\cards
| POST      | api\\story\\{story_id}\\card
| GET       | api\\story\\{story_id}\\tags
| POST      | api\\story\\{story_id}\\tag

### Index Cards
| Method    | Route
|---
| GET       | api\\card\\{card_id}
| DELETE    | api\\card\\{card_id}
| POST      | api\\card\\{card_id}\\position
| GET       | api\\card\\{card_id}\\author
| GET       | api\\card\\{card_id}\\tags
| POST      | api\\card\\{card_id}\\tags

### Tags
| Method    | Route
|---
| POST, PUT | api\\tag\\{tag_id}
